FROM openjdk:latest AS compile-java
WORKDIR /codechallenge
ARG JAR_FILE
COPY ${JAR_FILE} swagger-cli.jar
COPY ./swagger.json swagger.json
RUN java -jar swagger-cli.jar generate -i swagger.json -l nodejs-server -o nodejs

FROM node:latest AS node
COPY --from=compile-java /codechallenge/nodejs .
RUN npm install

EXPOSE 8080
CMD ["node", "index.js"]